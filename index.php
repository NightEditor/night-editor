<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/animate.css">
    <title>Night Editor - Download Program for windows</title>
  </head>
  <body>
<section class="container header">
  <h1>Night Editor</h1>
  <ul>
  <a href="/"><li><i class="fas fa-home"></i>Home</li></a>
  <a href="#program"><li><i class="fab fa-windows"></i>Program</li></a>
  <a href="#pro"><li style="color: #03A9F4;"><i class="far fa-fire"></i>PRO</li></a>
  </ul>
  <div class="left">
  <h1>Need to edit a video?</h1>
  <p>Then our Video Editor is ready to help you. Download the link below!</p>
<img src="/img/right.png" alt="">
  <a href="download.php"><button type="button" name="button"><i class="fab fa-windows"></i>Download Editor</button></a>
</div>
</section>
<section class="container programs" id="program">
  <b>About the program</b>
  <h1>The program is convenient to use.</h1>
  <p>Night Editor is intended for editing video files and creating videos of any complexity involving various visual and audio effects. The program offers rich functionality and yet has a simple and intuitive interface, allowing you to create videos with a bare minimum of efforts.</p>
    <a href="#pro"><button type="button" name="button"><i class="fab fa-windows"></i>View pro version</button></a>
  <img src="/img/redactor.png" alt="">
</section>
<section class="container pro" id="pro">
    <b>PRO VERSION</b>
    <h1>PRO VERSION is double convenient to use.</h1>
    <p>So it has huge functionality and all the amenities for use 
(Program For Windows 10/8/7 32- and 64-bit)</p>
    <a href="download.php"><button type="button" name="button"><i class="fab fa-windows"></i>Get Free Trial</button></a>
    <img src="/img/pro.png" alt="">
</body>
</html>
